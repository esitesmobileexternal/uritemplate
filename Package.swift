// swift-tools-version:5.0

import PackageDescription

let package = Package(
  name: "URITemplate",
  dependencies: [
    .package(url: "https://github.com/kylef/Spectre", .upToNextMajor(from: "0.7.0")),
    .package(url: "https://github.com/kylef/PathKit", .upToNextMajor(from: "0.7.0"))
],
  targets: [
    .target(
        name: "URITemplate",
        dependencies: [
            "Spectre",
            "PathKit"
        ],
        path: "Sources"
    )
    ]
)
